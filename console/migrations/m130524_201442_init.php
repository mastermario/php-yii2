<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $this->execute("
            DROP TABLE IF EXISTS {{%user}};
            CREATE TABLE {{%user}} (
                `id` int(11) NOT NULL,
                `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
                `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
                `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
                `status` smallint(6) NOT NULL DEFAULT '10',
                `created_at` int(11) NOT NULL,
                `updated_at` int(11) NOT NULL,
                `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

            ALTER TABLE {{%user}}
                ADD PRIMARY KEY (`id`),
                ADD UNIQUE KEY `username` (`username`),
                ADD UNIQUE KEY `email` (`email`),
                ADD UNIQUE KEY `password_reset_token` (`password_reset_token`),
                ADD KEY `phone` (`phone`),
                ADD KEY `type` (`type`);
        ");
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
