<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $phone
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property string $confirmPassword confirm password
 * @property string $type (self::TYPE_*) 
 */
class User extends \common\models\base\ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_DELETE = 'delete';
    const SCENARIO_CHANGE_PASSWORD = 'changePassword';

    const TYPE_UNLICENCED = 'unlicenced';
    const TYPE_LICENCED = 'licenced';
    const TYPE_OBSERVER = 'observer';

    public $password;
    public $confirmPassword;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            //create, update
            ['username', 'trim', 'on' => ['create', 'update']],
            ['username', 'required', 'on' => ['create', 'update']],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('app', 'This username has already been taken.'), 'on' => ['create', 'update']],
            ['username', 'string', 'min' => 2, 'max' => 255, 'on' => ['create', 'update']],

            ['email', 'trim', 'on' => ['create', 'update']],
            ['email', 'required', 'on' => ['create', 'update']],
            ['email', 'email', 'on' => ['create', 'update']],
            ['email', 'string', 'max' => 255, 'on' => ['create', 'update']],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('app', 'This email address has already been taken.'), 'on' => ['create', 'update']],

            //create, changePassword
            ['password', 'required', 'on' => ['create', 'changePassword']],
            ['password', 'string', 'min' => 8, 'on' => ['create', 'changePassword']],
            ['confirmPassword', 'required', 'on' => ['create', 'changePassword']],
            ['confirmPassword', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t('app', 'Passwords don\'t match'), 'on' => ['create', 'changePassword']],

            //create, update
            ['phone', 'trim', 'on' => ['create', 'update']],
            ['phone', 'required', 'on' => ['create', 'update']],
            ['phone', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('app', 'This phone has already been taken.'), 'on' => ['create', 'update']],
            ['phone', 'integer', 'integerOnly'=>true, 'on' => ['create', 'update']],
            ['phone', 'string', 'min' => 10, 'max' => 11, 'on' => ['create', 'update']],

            ['type', 'required', 'on' => ['create', 'update']],
            ['type', 'in', 'range' => [self::TYPE_UNLICENCED, self::TYPE_LICENCED, self::TYPE_OBSERVER], 'on' => ['create', 'update']],
            ['type', 'licencedValidator', 'on' => ['update']],

            ['status', 'default', 'value' => self::STATUS_ACTIVE, 'on' => ['create', 'update']],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED], 'on' => ['create', 'update']],

        ];
    }

    public function licencedValidator()
    {
        // code
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['username', 'email', 'password', 'phone', 'confirmPassword', 'type'];
        $scenarios[self::SCENARIO_UPDATE] = ['username', 'email', 'phone', 'type', 'status'];
        $scenarios[self::SCENARIO_CHANGE_PASSWORD] = ['password', 'confirmPassword'];
        return $scenarios;
    }

    public function getStatusLabel()
    {
        return $this->status === 0 ? Yii::t('app', 'Deleted') : Yii::t('app', 'Active');
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Id'),
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'status' => Yii::t('app', 'Status'),
            'password' => Yii::t('app', 'Password'),
            'confirmPassword' => Yii::t('app', 'Confirm password'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @inheritdoc
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\UserQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function beforeSave($insert)
    {
        // code
        parent::beforeSave($insert);
    }

    public function afterSave($insert)
    {
        // code
        return true;
    }

    public function beforeDelete()
    {
        // code
        return true;
    }



    public function transactions()
    {
        return [
            self::SCENARIO_DELETE => self::OP_DELETE,
            self::SCENARIO_UPDATE => self::OP_UPDATE,
        ];
    }
}
