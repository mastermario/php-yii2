<?php

namespace common\models\base;

use Yii;

/**
 * Base model \yii\db\ActiveRecord.
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public function delete()
    {
        try {
            return parent::delete();
        } catch (yii\db\IntegrityException $e) {
            if ($e->errorInfo[0] == '23000' && $e->errorInfo[1] == 1451) {
                Yii::$app->session->setFlash('error', Yii::t('app', 'The record has related data.'));
            }
            return false;
        }
    }

}
