<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\User */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\User;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if ($model->getScenario() === User::SCENARIO_CREATE): ?>
        
        <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'autofocus' => true]) ?>
        
        <?= $form->field($model, 'email') ?>

        <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'minlength' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput() ?>
        
        <?= $form->field($model, 'confirmPassword')->passwordInput() ?>

        <?php 
            echo $form->field($model, 'type')->widget(Select2::classname(), [
                'data' => [User::TYPE_UNLICENCED => User::TYPE_UNLICENCED, User::TYPE_LICENCED => User::TYPE_LICENCED, User::TYPE_OBSERVER => User::TYPE_OBSERVER],
                'options' => ['placeholder' => Yii::t('app', 'Select a state ...')],
                'pluginOptions' => [
                    'allowClear' => false
                ],
            ]); 
        ?>

    <?php elseif ($model->getScenario() === User::SCENARIO_UPDATE): ?>

        <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'autofocus' => true]) ?>
            
        <?= $form->field($model, 'email') ?>

        <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'minlength' => true]) ?>

        <?php 
            echo $form->field($model, 'status')->dropDownList([0 => Yii::t('app', 'Deleted'), 10 => Yii::t('app', 'Active')]);
        ?>

        <?php 
            echo $form->field($model, 'type')->widget(Select2::classname(), [
                'data' => [User::TYPE_UNLICENCED => User::TYPE_UNLICENCED, User::TYPE_LICENCED => User::TYPE_LICENCED, User::TYPE_OBSERVER => User::TYPE_OBSERVER],
                'options' => ['placeholder' => Yii::t('app', 'Select a state ...')],
                'pluginOptions' => [
                    'allowClear' => false
                ],
            ]); 
        ?>
    <?php elseif ($model->getScenario() === User::SCENARIO_CHANGE_PASSWORD): ?>

        <?= $form->field($model, 'password')->passwordInput() ?>
        
        <?= $form->field($model, 'confirmPassword')->passwordInput() ?>

    <?php endif; ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
